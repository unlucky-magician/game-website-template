# Game Website Template

This is being designed to make it easier for game developers to get a website up and running as quick as possible as well as minimising the cost of doing so. The current plan is to do this using Jekyll and Github pages since it allows completely free website hosting as well as making it easy to set up with a custom domain and a SSL certificate.

# Features

Game Website Template is very much in a concept phase and is likely to change direction at a moments notice. A design is still being worked on however **a minimal set of features will include**:

- Ability to set a header image and override the default styling and colours of everything
- A number of feature sections which allow you to commicate what your game is about
- Being able to set a Youtube link for the game trailer
- Showing a Steam store widget for the game
- Showing an itch.io store widget for the game
- Google Analytics integration
- Being able to set social links for Facebook, Twitter, Instagram and Youtube
- Development blogging
- Separate Presskit page (not to be confused with presskit())
- Fully responsive so that it renders nicely on all kinds of devices
- Room for a number of screenshots
- Link back to company website
- Ability to set an image as the background
- Awards section
- User testimonials section

Some features that would be great but probably wont be included in an initial version include:

- Mailchimp integration for mailing lists
- Humble Store widget
- Google Play store widget
- App Store widget
- Carousel widget to have a rotation slideshow of images
- Links to Xbox One store and PlayStation Store
- Search Engine Optimisation tags

# Feedback or want to get involved?

If you have any ideas, want to get involved or have a design in mind feel free to contact me on timothy.veletta(at)gmail.com.
